var fs = require('fs');
var URL = require('url');
var Utils = require('../Utils/Utils');

function ResponseAsset(path, response)
{
    if (!fs.existsSync(path))
    {
        console.log('Error, file does not exist.', path);
    }
    else
    {
        try
        {
            var stat = fs.statSync(path);
            if (stat.isDirectory())
            {
                console.log('Is Directory');
            }

            response.writeHead(200, { 'Content-Type': Utils.GetContentTypeOfPath(), 'Content-Length': stat.size});
            var readStream = fs.createReadStream(path);
            readStream.pipe(response);
        }
        catch (ex)
        {
            console.log(ex);
        }
    }
}

exports.GET = function (connection)
{
    var filename = URL.parse(connection.httpRequest.url, true)['pathname'];
    var pathname = "../" + filename;
    ResponseAsset(pathname, connection.httpResponse);
};
