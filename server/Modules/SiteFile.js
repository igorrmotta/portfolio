/**
 * Created by enrique on 08/10/2014.
 */
var fs = require('fs');
var URL = require('url');
var Utils = require('../Utils/Utils');
var JsonCode = require('../Utils/JsonCode');

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.GET = function (connection)
{
    var filename = URL.parse(connection.httpRequest.url, true)['pathname'];

    if (filename == '/')
    {
        filename = "/site/html/app.html";
    }

    //Modificar html
    var pathname = "../" + filename;

    fs.readFile(pathname, 'utf-8', function (err, data)
    {
        if (err)
        {
            console.log("Error: ", err);
            var strError = "Internal server error.";
            Utils.ResponseError(connection, strError, JsonCode.FAIL);
            return;
        }
        Utils.ResponseSiteFile(connection, data, Utils.GetContentTypeOfPath(pathname));
    });
};

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.POST = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.PUT = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.DELETE = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};