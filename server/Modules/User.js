/**
 * Created by enrique on 08/10/2014.
 */

/* Connection***

 connection.httpRequest = request;
 connection.httpResponse = response;
 connection.httpMethod = request.method.toUpperCase();
 connection.payload = data;
 connection.isUserAuthenticated

 */
var Database = require('./../InterfaceDB');
var JsonCode = require('./../Utils/JsonCode');
var Utils = require('./../Utils/Utils');
var m_connection;


/*
 * Requests
 */
var onRequestDataCompleted = function(jsonResults)
{
    Utils.ResponseSuccess(m_connection, jsonResults);
};

var onRequestDataFailed = function(strError, code)
{
    Utils.ResponseError(m_connection, strError, code);
};


/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.GET = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.POST = function (connection)
{
    console.log("User POST -", (new Date()).toLocaleString());

    //Route /api/user
    m_connection = connection;
    var payload = connection.payload;

    var user = Utils.CreateUser();
    user.name = payload['name'];
    user.email = payload['email'];
    user.encrypt = payload['encrypt'];

    Database.AddUser(user, onRequestDataCompleted, onRequestDataFailed);
};

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.PUT = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.DELETE = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};