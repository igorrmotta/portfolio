/**
 * Created by enrique on 08/10/2014.
 */

var spawn = require('child_process').spawn;

function RunPowerShell(args)
{
    var child = spawn('powershell.exe', [args]);
    console.log("\n\nExecute powershell: " + args);
    child.stdout.on("data", function (data)
    {
        console.log("Powershell Data: " + data);
    });

    child.stderr.on("data", function (data)
    {
        console.log("Powershell Errors: " + data);
    });

    child.on("exit", function ()
    {
        console.log("Powershell Script Finished!");
    });

    child.stdin.end();
}

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.GET = function (connection)
{
    Utils.GetLastPort(onValidPortCompleted, onValidPortFailed);

    function onValidPortCompleted(port)
    {
        var objServer = Utils.CreateServer();
        objServer.users_iduser = 1; // token
        objServer.file_info_version = 1; // Utils
        objServer.port = port;
        objServer.application_name = "User2"; //url

        //Optional
        objServer.ssl_port = 443; // Nao utilizado
        objServer.sys_user_encryption = "acddd3306198ba9a108c80c775a705a901e259ee"; // token
        objServer.sys_user = "sys"; // token
        objServer.ssl_certificate = "testcert.crt"; // Utils
        objServer.ssl_private_key = "testprivkey.key"; // Utils
        objServer.ssl_enable = "false";
        objServer.smtp_server = "";
        objServer.smtp_user = "";
        objServer.smtp_password = "";
        objServer.elipse_demo_mode = -1;
        objServer.experimental = -1;

        exports.CreateMobileServer(
            objServer.application_name,
            objServer,
            function ()
            {
                console.log("sucesso");

                connection.httpResponse.writeHead(200);
                connection.httpResponse.end();
            },
            function (strError, code)
            {
                console.log("falhou! ", strError);

                connection.httpResponse.writeHead(400);
                connection.httpResponse.end();
            });

    }

    function onValidPortFailed(strError, code)
    {

    }



    /*
     RunPowerShell('/helloworld.ps1');
     */
};

var Utils = require('../Utils/Utils');
var JsonCode = require('../Utils/JsonCode');
var Fs = require('fs');

exports.CreateMobileServer = function (appName, objServer, onCreateCompleted, onCreateFailed)
{


    var strError = "";
    if (!Utils.IsValidServer(objServer, strError))
    {
        onCreateCompleted(strError, JsonCode.FAIL);
        return;
    }

    exports.CreateNewServer(
        appName,
        function (pathDest)
        {
            OnCreateNewServerCompleted(pathDest, objServer, onCreateCompleted, onCreateFailed)
        },
        onCreateFailed);

    function OnCreateNewServerCompleted(pathDest, objServer, onChangeInitCompleted, onChangeInitFailed)
    {
        exports.ChangeInitFile(
            pathDest,
            objServer,
            function (pathDest)
            {
                OnChangeInitFileCompleted(pathDest, objServer, onChangeInitCompleted, onChangeInitFailed)
            },
            function (strError, code)
            {
                deleteFolderRecursive(pathDest);
                onChangeInitFailed(strError, code);
            });
    }

    function OnChangeInitFileCompleted(pathDest, objServer, onRunServerCompleted, onRunServerFailed)
    {
        exports.RunServer(
            pathDest,
            objServer,
            onRunServerCompleted,
            function (strError, code)
            {
                deleteFolderRecursive(pathDest);
                onRunServerFailed(strError, code);
            });
    }
};

function deleteFolderRecursive(path)
{
    var files = [];
    if (Fs.existsSync(path))
    {
        files = Fs.readdirSync(path);
        files.forEach(function (file, index)
        {
            var curPath = path + "/" + file;
            if (Fs.lstatSync(curPath).isDirectory())
            { // recurse
                deleteFolderRecursive(curPath);
            }
            else
            { // delete file
                Fs.unlinkSync(curPath);
            }
        });
        Fs.rmdirSync(path);
    }
}

exports.CreateNewServer = function (appName, onCreateCompleted, onCreateFailed)
{
    var pathTemplate = Utils.GetMobileTemplatePath();
    var pathDest = Utils.GetNewMobilePath(appName);

    if (!Utils.CopyRecursiveSync(pathTemplate, pathDest))
    {
        var strError = "Error to create new project: folder already exist.";
        onCreateFailed(strError, JsonCode.FAIL);
    }
    else
    {
        onCreateCompleted(pathDest);
    }
};

exports.ChangeInitFile = function (pathDest, objServer, onCreateCompleted, onCreateFailed)
{
    var strInit = Utils.CreateInitFile(objServer);
    var pathInit = Utils.GetNewInitPath(pathDest);

    Fs.writeFile(pathInit, strInit, function (error)
    {
        if (error)
        {
            console.log("Error WriteFile: ", error);
            var strError = "Error to create init file";
            onCreateFailed(strError, JsonCode.FAIL);
        }
        else
        {
            onCreateCompleted(pathDest);
        }
    });
};

exports.RunServer = function (pathDest, objServer, onCreateCompleted, onCreateFailed)
{
    var port = objServer.port;
    var command = Utils.GetRunServerCommand(pathDest, port);

    var exec = require('child_process').exec;
    exec(command, function (error, stdout, stderr)
    {
        console.log("stdout: ", stdout);
        console.log("stderr: ", stderr);
        if (error || stderr)
        {
            console.log("Error to execute command: ", error, stderr);
            var strError = "Error to execute Mobile Server command.";
            onCreateFailed(strError, JsonCode.FAIL);
        }
        else
        {
            onCreateCompleted();
        }
    });
};


/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.POST = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.PUT = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.DELETE = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};