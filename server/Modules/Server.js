/* Connection***

 connection.httpRequest = request;
 connection.httpResponse = response;
 connection.httpMethod = request.method.toUpperCase();
 connection.payload = data;
 connection.isUserAuthenticated

 */
var Database = require('./../InterfaceDB');
var JsonCode = require('./../Utils/JsonCode');
var Utils = require('./../Utils/Utils');
var m_connection;


/*
 * Requests
 */
var onRequestDataCompleted = function(jsonResults)
{
    Utils.ResponseSuccess(m_connection, jsonResults);
};

var onRequestDataFailed = function(strError, code)
{
    Utils.ResponseError(m_connection, strError, code);
};

exports.GET = function (connection)
{
    var cookies = Utils.parseCookies(connection.httpRequest);
    var token = cookies["emc_token"];
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};

exports.POST = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};

exports.PUT = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};

exports.DELETE = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};