/**
 * Created by enrique on 08/10/2014.
 */

/* Connection***

 connection.httpRequest = request;
 connection.httpResponse = response;
 connection.httpMethod = request.method.toUpperCase();
 connection.payload = data;
 connection.isUserAuthenticated

 */

var Database = require('./../InterfaceDB');
var Utils = require('./../Utils/Utils');
var JsonCode = require('./../Utils/JsonCode');

var m_connection;


/*
 * Requests
 */
var onRequestDataCompleted = function (jsonResults)
{
    Utils.ResponseSuccess(m_connection, jsonResults);
};

var onRequestDataFailed = function (strError, code)
{
    Utils.ResponseError(m_connection, strError, code);
};


/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.GET = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.POST = function (connection)
{
    console.log("Login -", (new Date()).toLocaleString());
    /*
     URL: /api/login
     */
    m_connection = connection;
    var payload = connection.payload;
    var encrypt = payload['encrypt'] ? payload['encrypt'] : "";
    var email = payload['email'] ? payload['email'] : "";

    Database.CheckLogin(encrypt, email, function (user)
    {
        //TODO gerar token para o client salvar no cookie
        //user.token = user.email;
        console.log(user);
        var resp = {};
        resp.name = user.name;
        resp.email = user.email;
        resp.token = user.email;
        console.log(resp);
        onRequestDataCompleted(resp);

    }, function (strError, code)
    {
        onRequestDataFailed(strError, code);
    });
};

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.PUT = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.DELETE = function (connection)
{
    m_connection = connection;
    onRequestDataFailed("Service not found.", JsonCode.NOT_FOUND);
};