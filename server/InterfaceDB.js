/**
 * Created by enrique on 08/10/2014.
 */

var JsonCode = require('./Utils/JsonCode');
var db = require('./MySQLDB');
db.CreateConnection();
db = db.GetDb();

var m_tableUsers = "users";
var m_columnEncrypt = "encrypt";
var m_columnEmail = "email";

/*
 * @void
 * @onRequestDataCompleted (json)
 * @onRequestDataFailed (strError, code)
 */
exports.AddUser = function (user, onRequestDataCompleted, onRequestDataFailed)
{
    var strError = "";
    if (!Utils.IsValidUser(user, strError))
    {
        onRequestDataFailed(strError, JsonCode.FAIL);
        return;
    }

    CheckUser(user.email, function ()
    {
        var sQuery = "INSERT INTO " +
            m_tableUsers +
            " SET ?;";

        console.log("  SQL - AddUser: \n   ", sQuery);

        db.query(sQuery, user, function (errorUser, result)
        {
            if (errorUser)
            {
                console.log(errorUser);
                strError = "Internal server error. (Db - " + errorUser.errno + ")";
                onRequestDataFailed(errorUser, JsonCode.FAIL);
            }
            else
            {
                console.log("Add user: ", result);
                onRequestDataCompleted({ idUser: result.insertId });
            }
        });
    }, onRequestDataFailed);
};

CheckUser = function (email, onValidLogin, onInvalidLogin)
{
    var strError = "";
    if (!email || email == "")
    {
        strError = "Invalid email.";
        onRequestDataFailed(strError, JsonCode.FAIL);
        return;
    }

    var sQuery = "SELECT * FROM " +
        m_tableUsers +
        " WHERE " + m_columnEmail +" = " + db.escape(email) + ";";

    console.log("  SQL - CheckEncrypt: \n   ", sQuery);

    db.query(sQuery, function (error, results)
    {
        if (error)
        {
            console.log(error);
            strError = "Internal server error. (Db - " + error.errno + ")";
            onRequestDataFailed(strError, JsonCode.FAIL);
        }
        else
        {
            if (results.length > 0)
            {
                var strError = "User already exists.";
                onInvalidLogin(strError, JsonCode.FAIL);
            }
            else
            {
                onValidLogin();
            }
        }
    });
};

exports.CheckLogin = function (encrypt, email, onRequestDataCompleted, onRequestDataFailed)
{
    var strError = "";
    if (!encrypt || encrypt == "")
    {
        strError = "Invalid encryption.";
        onRequestDataFailed(strError, JsonCode.FAIL);
        return;
    }
    if (!email || email == "")
    {
        strError = "Invalid email.";
        onRequestDataFailed(strError, JsonCode.FAIL);
        return;
    }

    var sQuery = "SELECT * FROM " +
        m_tableUsers +
        " WHERE " +  m_columnEncrypt + " = " + db.escape(encrypt) +
        " AND " +  m_columnEmail + " = " + db.escape(email) + ";";

    console.log("  SQL - CheckUser: \n   ", sQuery);

    db.query(sQuery, function (error, result)
    {
        if (error)
        {
            console.log(error);
            strError = "Internal server error. (Db - " + error.errno + ")";
            onRequestDataFailed(strError, JsonCode.FAIL);
        }
        else
        {
            if (result.length <= 0)
            {
                var strError = "Invalid user / password.";
                onRequestDataFailed(strError, JsonCode.UNAUTHORIZED);
            }
            else
            {
                console.log("Check user: ", result);
                onRequestDataCompleted(result[0]);
            }
        }
    });
};


exports.GetMaxPort = function(onRequestDataCompleted, onRequestDataFailed)
{
    var sqlPort = "SELECT MAX(port) FROM servers";
    console.log("  SQL - GetMaxUser: \n   ", sqlPort);

    db.query(sqlPort, function (error, port)
    {
        if (error)
        {
            console.log(error);
            var strError = "Internal server error. (Db - " + error.errno + ")";
            onRequestDataFailed(strError, JsonCode.FAIL);
        }
        else
        {
            onRequestDataCompleted(port);
        }
    });
};

exports.GetServerList = function(userId, onRequestDataCompleted, onRequestDataFailed)
{
    userId = userId|0;

    if(userId < 1){
        onRequestDataFailed("Invalid user", JsonCode.FAIL);
        return;
    }

    var sqlServer = "SELECT * FROM servers WHERE users_iduser = " + db.escape(userId);

    db.query(sqlServer, function (error, result)
    {
        if (error)
        {
            console.log(error);
            var strError = "Internal server error. (Db - " + error.errno + ")";
            onRequestDataFailed(strError, JsonCode.FAIL);
        }
        else
        {
            onRequestDataCompleted(result);
        }
    });
};