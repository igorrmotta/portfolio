/**
 * Created by enrique on 08/10/2014.
 */

var QueryString = require('querystring');
var Path = require('path');
var Fs = require('fs');
var Net = require('net');
/**
 * @void
 */
exports.ResponseSuccess = function (connection, result)
{
    //TODO: Se vier vazio, manda uma resposta vazia ou sei la
    var jsonResponse = result;
    var httpCode = 200; //Sucess

    console.log("-->> ResponseSuccess");
    exports.ResponseJson(connection, jsonResponse, httpCode);
};


/**
 * @void
 */
exports.ResponseError = function (connection, strError, code)
{
    var jsonResponse = { Message: strError, Code: code};
    var httpCode = 400;

    // Exemplo
    /*switch (code)
     {
     case JsonCode.UNAUTHORIZED:
     {
     httpCode = 401; // Unauthorized
     break;
     }
     case JsonCode.NOT_FOUND:
     {
     httpCode = 404; // Service Not Found
     break;
     }
     case JsonCode.INTERNAL:
     {
     httpCode = 500; // Internal Server Error
     }
     }*/

    console.log("-->> ResponseError: \n", jsonResponse);
    exports.ResponseJson(connection, jsonResponse, httpCode);
};


/**
 * @void
 */
exports.ResponseJson = function (connection, responseJson, httpCode, headerOpt)
{
    var header = {
        'Content-Type': 'application/json; charset=utf-8'
    };

    for (var key in headerOpt)
    {
        header[key] = headerOpt[key];
    }

    var response = connection.httpResponse;
    response.writeHead(httpCode, header);
    response.end(JSON.stringify(responseJson));
};


/**
 * @void
 */
exports.ResponseSiteFile = function (connection, file, contentType)
{
    console.log("-->> ResponseSiteFileSuccess");
    var response = connection.httpResponse;
    response.setHeader("Content-Type", contentType);
    response.serverStatus = 200;
    response.end(file);
};


/**
 * @string
 */
exports.GetContentTypeOfPath = function (pathName)
{
    var extension = Path.extname(pathName);
    var contentType;
    switch (extension)
    {
        case ".js":
            contentType = 'application/javascript';
            break;
        case ".html":
            contentType = 'text/html';
            break;
        case ".css":
            contentType = 'text/css';
            break;
        case ".woff":
        case ".woff2":
            contentType = 'application/font-woff';
            break;
        case ".eot":
            contentType = 'application/vnd.ms-fontobject';
            break;
        case ".svg":
            contentType = 'application/svg+xml';
            break;
        case ".ttf":
            contentType = 'application/x-font-ttf';
            break;
        case ".png":
            contentType = 'image/png';
            break;
        case ".svg":
            contentType = 'image/svg+xml'
            break;
        default:
            break;
    }
    return contentType;
};


/**
 * @void
 */
exports.GetPostData = function (connection, onRequestDataCompleted, onRequestDataFailed)
{
    var request = connection.httpRequest;
    var data = '';

    request.on('data', function (chunk)
    {
        data += chunk;
    });

    request.on('end', function ()
    {
        onRequestDataCompleted(QueryString.parse(data));
    });

    request.on('error', function (err)
    {
        onRequestDataFailed(err);
    });
};


/**
 * @return {string} path The path of Mobile template.
 */
exports.GetMobileTemplatePath = function ()
{
    return Path.relative(__dirname, "C:/MobileCloud/Template");
};


/**
 * @param {string} newPath
 * @return {string} newPath
 */
exports.GetNewMobilePath = function (newPath)
{
    var mobileServerPath = Path.relative(__dirname, "C:/MobileCloud/");
    return Path.join(mobileServerPath, newPath);
};


/**
 * @param {string} newPath
 * @return {string} newPath
 */
exports.GetNewInitPath = function (newPath)
{
    return Path.join(newPath, "Elipse Mobile Server/Init.txt");
};


/**
 * @param {string} src The path to the thing to copy.
 * @param {string} dest The path to the new copy.
 * @return {boolean}
 */
exports.CopyRecursiveSync = function (src, dest)
{
    var exists = Fs.existsSync(src);
    var stats = exists && Fs.statSync(src);
    var isDirectory = exists && stats.isDirectory();
    if (Fs.existsSync(dest))
    {
        return false;
    }
    if (exists && isDirectory)
    {
        Fs.mkdirSync(dest);
        Fs.readdirSync(src).forEach(function (childItemName)
        {
            exports.CopyRecursiveSync(Path.join(src, childItemName), Path.join(dest, childItemName));
        });
    }
    else
    {
        Fs.linkSync(src, dest);
    }
    return true;
};


/**
 * @param {object} objServer
 * @return {string}
 */
exports.CreateInitFile = function (objServer)
{
    var strInit = "";
    strInit += "version=1" + ";\r\n";
    strInit += "port=" + objServer.port + ";\r\n";
    strInit += "encryption=" + objServer.sys_user_encryption + ";\r\n";
    strInit += "sysuser=" + objServer.sys_user + ";\r\n";
    strInit += "appname=" + objServer.application_name + ";\r\n";
    strInit += "certificate=" + objServer.ssl_certificate + ";\r\n";
    strInit += "privkey=" + objServer.ssl_private_key + ";\r\n";
    strInit += "sslport=" + objServer.ssl_port + ";\r\n";
    strInit += "sslenabled=" + objServer.ssl_enable + ";\r\n";
    return strInit;
};

/**
 * @return {string}
 */
exports.GetRunServerCommand = function (pathDest, port)
{
    pathDest = Path.join(pathDest, "Elipse Mobile Server");
    return "cd " + pathDest + " & " + "MobileServer.exe init.txt -run";
};


exports.GetUri = function (url)
{
    return url.split('?')[0];
};


/**
 * @return {object} user
 */
exports.CreateUser = function ()
{
    var user = {};
    user.name = '';
    user.encrypt = '';
    user.email = '';
    return user;
};


/**
 * @return {object} server
 */
exports.CreateServer = function ()
{
    var server = {};
    server.users_iduser = -1;
    server.file_info_version = -1;
    server.port = -1;
    server.application_name = "";

    //Optional
    server.ssl_port = -1;
    server.sys_user_encryption = "";
    server.sys_user = "";
    server.ssl_certificate = "";
    server.ssl_private_key = "";
    server.ssl_enable = -1;
    server.smtp_server = "";
    server.smtp_user = "";
    server.smtp_password = "";
    server.elipse_demo_mode = -1;
    server.experimental = -1;

    return server;
};

/**
 * @param {user} user
 * @param {string} strError
 * @return {boolean}
 */
exports.IsValidUser = function (user, strError)
{
    strError = ""; // out
    if (!user)
    {
        strError = "Internal server error, invalid user.";
        return false;
    }
    if (!user.hasOwnProperty('name') || user.name == "")
    {
        strError = "Invalid name.";
        return false;
    }
    if (!user.hasOwnProperty('encrypt') || user.encrypt == "")
    {
        strError = "Invalid encryption.";
        return false;
    }
    if (!user.hasOwnProperty('email') || user.email == "")
    {
        strError = "Invalid email.";
        return false;
    }
    return true;
};

/**
 * @param {user} objServer
 * @param {string} strError
 * @return {boolean}
 */
exports.IsValidServer = function (objServer, strError)
{
    strError = ""; // out
    if (!objServer)
    {
        strError = "Internal server error, invalid objServer.";
        return false;
    }
    if (!objServer.hasOwnProperty('users_iduser')
        || objServer.users_iduser == -1
        || objServer.users_iduser == "")
    {
        strError = "Internal server error, invalid users_iduser.";
        return false;
    }
    if (!objServer.hasOwnProperty('file_info_version')
        || objServer.file_info_version == -1
        || objServer.file_info_version == "")
    {
        strError = "Internal server error, invalid file_info_version.";
        return false;
    }
    if (!objServer.hasOwnProperty('port')
        || objServer.port == -1
        || objServer.port == "")
    {
        strError = "Internal server error, invalid port.";
        return false;
    }
//    if (!objServer.hasOwnProperty('ssl_port')
//        || objServer.ssl_port == -1
//        || objServer.ssl_port == "")
//    {
//        strError = "Internal server error, invalid ssl_port.";
//        return false;
//    }
    if (!objServer.hasOwnProperty('application_name') || objServer.application_name == "")
    {
        strError = "Invalid application name.";
        return false;
    }
    return true;
};


var lastPort = 51000;

exports.GetMinPort = 51001;
exports.GetMaxPort = 52000;

exports.GetLastPort = function (onValidPortCompleted, onValidPortFailed)
{
    if (lastPort != this.GetMinPort)
    {
        onPort(lastPort);
    }
    else
    {
        require('../InterfaceDB').GetMaxPort(
            function (port)
            {
                lastPort = port;
                onPort(port);
            },
            function (strError, code)
            {
                onPort(this.GetMinPort);
            }
        );
    }

    function onPort(lastPort)
    {
        IsValidPort(lastPort, onValidPortCompleted, onValidPortFailed);
    }
};

function IsValidPort(port, onValidPort, onInvalidPort)
{
    if (port > this.GetMaxPort)
    {
        var strError = "Internal server error, port limit reached.";
        onInvalidPort(strError, JsonCode.FAIL);
        return;
    }

    var sock = new Net.Socket();
    sock.setTimeout(2500);
    sock.on('connect', function ()
    {
        console.log('localhost' + ':' + port + ' is up.');
        sock.destroy();
        IsValidPort(++port, onValidPort, onInvalidPort);
    }).on('error', function (e)
    {
        console.log('localhost' + ':' + port + ' is down: ' + e.message);
        onValidPort(port);
    }).on('timeout', function (e)
    {
        console.log('localhost' + ':' + port + ' is down: timeout');
        IsValidPort(++port, onValidPort, onInvalidPort);
    }).connect(port, 'localhost');
}

exports.parseCookies = function (request)
{
    var list = {},
        rc = request.headers.cookie;

    rc && rc.split(';').forEach(function (cookie)
    {
        var parts = cookie.split('=');
        list[parts.shift().trim()] = unescape(parts.join('='));
    });

    return list;
}