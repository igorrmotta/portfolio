/**
 * Created by enrique on 08/10/2014.
 */

Codes = {
    SUCCESS:  0,
    FAIL: 1,
    INVALID_TOKEN: 2,
    DISCONNECTED:  3,
    NO_LICENSE: 4,
    UNAUTHORIZED:  5,
    NOT_FOUND: 6,
    CONNECTION_ERROR: 100
};

module.exports = Codes;
