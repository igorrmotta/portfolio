var http = require('http'),
    URL = require('url'),
    fs = require('fs'),
    Utils = require('./Utils/Utils'),
    JsonCode = require('./Utils/JsonCode');


var objSiteFile = require('./Modules/SiteFile');
var objAssetsFile = require('./Modules/AssetsFile');
var objPShelFile = require('./Modules/PowerShellFile');
var objUser = require('./Modules/User');
var objLogin = require('./Modules/Login');
var objServer = require('./Modules/Server');


function Dispath(serviceObj, connection)
{
    if (serviceObj && serviceObj[connection.httpMethod])
    {
        serviceObj[connection.httpMethod](connection);
    }
}

function CallIfExists(serviceObj, connection)
{
    if (!serviceObj || serviceObj == '')
    {
        var strError = "Service not found.";
        Utils.ResponseError(connection, strError, 1);
        return;
    }

    if (connection.httpMethod == 'POST' || connection.httpMethod == 'PUT')
    {
        var onRequestDataCompleted = function (data)
        {
            connection.payload = data;
            Dispath(serviceObj, connection);
        };

        var onRequestDataFailed = function ()
        {
            console.log('Fail to Read POST/PUT data. HttpStatus 500');
            Utils.ResponseJson(m_connection, "Fail to Read POST/PUT data.", JsonCode.FAIL);
        };

        Utils.GetPostData(connection, onRequestDataCompleted, onRequestDataFailed);
    }
    else
    {
        connection.payload = URL.parse(connection.httpRequest.url, true)['query'];
        Dispath(serviceObj, connection);
    }
}

var server = http.createServer(function (request, response)
{
    if (request.url == '/favicon.ico')
    {
        response.end();
        return;
    }

    var connection = {};
    connection.httpRequest = request;
    connection.httpResponse = response;
    connection.httpMethod = request.method.toUpperCase();
    connection.payload = {};
    //connection.isUserAuthenticated = true;


    var url = Utils.GetUri(request.url);
    url = url.split("/");

    if (url[0] == '')
    {
        var obj;

        // Route : ˆ/*
        if (url[1] == '')
        {
            obj = objSiteFile;
        }
        //Route : ^/App.html/*
        else if (url[1] == 'App.html')
        {
            obj = objSiteFile;
        }
        //Route : ˆ/site/*
        else if (url[1] == 'site')
        {
            if (url[2] == 'assets' || url[2] == 'webfont-kit')
            {
                obj = objAssetsFile;
            }
            else
            {
                obj = objSiteFile;
            }
        }
        //Route : ˆ/api/*
        else if (url[1] == 'api')
        {
            var pathname = URL.parse(request.url, true)['pathname'];
            var service = pathname.substring('/api/'.length);

            switch (service)
            {
                case 'powershell':
                    obj = objPShelFile;
                    break;
                case 'user':
                    obj = objUser;
                    break;
                case 'login':
                    obj = objLogin;
                    break;
                case 'server':
                    obj = objServer;
                    break;
                default :
                    break;
            }
        }

        CallIfExists(obj, connection);
    }
    else
    {
        response.writeHead(404, {'Content-type': 'application/json'});
        response.end(JSON.stringify({message: 'Service not found'}));
    }
});

server.listen(3000);
console.log('Server is running at localhost:3000');