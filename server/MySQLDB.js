/**
 * Created by enrique on 08/10/2014.
 */

var Db = require('mysql');
var Config = require('./Config');

var dbConnection;
exports.CreateConnection = function ()
{
    dbConnection = Db.createConnection({
        host : Config.dbHost,
        port : Config.dbPort,
        user : Config.dbUser,
        password : Config.dbPassword,
        database : Config.dbDatabase
    });
};

exports.GetDb = function ()
{
    return dbConnection;
};

exports.CloseDb = function ()
{
    dbConnection.end();
};


