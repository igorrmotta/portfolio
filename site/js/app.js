$(document).ready(function()
				  {
					  var firstPage = CreateFirstPageElement();
					  var secondPage = CreateSecondPageElement();
					  var thirdPage = CreateThirdPageElement();

					  var pages = [
						  firstPage,
						  secondPage,
						  thirdPage
					  ];
					  AdjustZOrder(pages);

					  var pageViewer = CreatePageViewer(pages);

					  document.body.appendChild(pageViewer);
				  });

function AdjustZOrder(vecElements)
{
	var zIndex = vecElements.length;
	for(var index in vecElements)
	{
		var elem = vecElements[index];
		elem.style.zIndex = zIndex;
		zIndex--;
	}
}

