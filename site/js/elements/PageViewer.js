function CreatePageViewer(pageElements)
{
    var _this = document.createElement('div');
    _this.className = 'pageViewer';
    _this.style.width = '100%';
    _this.style.height = '100%';
    _this.style.position = 'fixed';

    _this.pages = pageElements;
    _this.iCurrentPageIndex = 0;

    _this.btnLeft = document.createElement('button');
    _this.btnLeft.style.position = 'relative';
    _this.btnLeft.style.float = 'left';
    _this.btnLeft.style.width = '200px';
    _this.btnLeft.style.top = '50%';
    _this.btnLeft.style.height = '80px';
    _this.btnLeft.style.zIndex = '9999999999';
    _this.btnLeft.style.backgroundColor = '#000000';
    _this.btnLeft.style.color = '#FFFFFF';
    _this.btnLeft.textContent = "PREVIOUS";
    _this.btnLeft.onclick = function (event)
    {
        PreviousPage();
    };
    _this.appendChild(_this.btnLeft);

    _this.btnRight = document.createElement('button');
    _this.btnRight.style.position = 'relative';
    _this.btnRight.style.float = 'right';
    _this.btnRight.style.verticalAlign = 'middle';
    _this.btnRight.style.top = '50%';
    _this.btnRight.style.width = '200px';
    _this.btnRight.style.height = '80px';
    _this.btnRight.style.zIndex = '9999999999';
    _this.btnRight.style.backgroundColor = '#000000';
    _this.btnRight.style.color = '#FFFFFF';
    _this.btnRight.textContent = "NEXT";

    _this.btnRight.onclick = function (event)
    {
        NextPage();
    };
    _this.appendChild(_this.btnRight);

    for (var index in _this.pages)
    {
        _this.appendChild(_this.pages[index]);
    }

    function NextPage()
    {
        if (_this.iCurrentPageIndex == _this.pages.length - 1)
        {
            return;
        }

        _this.pages[_this.iCurrentPageIndex].style.width = '0%';
        _this.iCurrentPageIndex++;
    }

    function PreviousPage()
    {
        if (_this.iCurrentPageIndex == 0)
        {
            return;
        }

        _this.iCurrentPageIndex--;
        _this.pages[_this.iCurrentPageIndex].style.width = '100%';
    }

    return _this;
}
