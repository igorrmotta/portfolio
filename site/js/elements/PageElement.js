function CreateFirstPageElement()
{
    var _this = document.createElement('div');
    _this.className = "first";
    var canvas = document.createElement('canvas');
    _this.appendChild(canvas);

    return _this;
}

function CreateSecondPageElement()
{
    var _this = document.createElement('div');
    _this.className = "second";
    return _this;
}

function CreateThirdPageElement()
{
    var _this = document.createElement('div');
    _this.className = "third";
    return _this;
}